<?php

namespace Compucie\Touchscreen\ShopPage\Model;

use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;
use Compucie\Touchscreen\Main\Model\Session;

class ShopSession extends Session
{
    public const SELECTED_FOLDER_ID = "selectedFolderId";
    public const SHOPPING_BASKET = "shoppingBasket";

    public function clear(): void
    {
        $this->setVars([self::SELECTED_FOLDER_ID => null,
            self::SHOPPING_BASKET => array(),
        ]);
    }

    public function manage(): void
    {
        // Parse GET parameters
        $this->setNotNull(self::SELECTED_FOLDER_ID, $_GET[self::SELECTED_FOLDER_ID] ?? null);

        // Cleanup
        if (SessionManager::getPageType() != PageType::SHOP) $this->unset(self::SELECTED_FOLDER_ID);
    }

    public function getSelectedFolderId(): ?int
    {
        return $this->get(self::SELECTED_FOLDER_ID);
    }

    public function getShoppingBasket(): ?array
    {
        return $this->get(self::SHOPPING_BASKET);
    }

    public function setShoppingBasket(array $shoppingBasket): void
    {
        $this->set(self::SHOPPING_BASKET, $shoppingBasket);
    }
}
