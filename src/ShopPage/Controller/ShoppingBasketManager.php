<?php

namespace Compucie\Touchscreen\ShopPage\Controller;

use Compucie\Congressus\Model\Product;

use Compucie\Touchscreen\Authentication\Controller\RfidAuthenticator;
use Compucie\Touchscreen\Main\Controller\SessionManager;

use function Compucie\Touchscreen\createCongressusClient;

/**
 * Managed session variables: shoppingBasket
 * The session variable `shoppingBasket` is an array in which each key is a
 * product ID and each value is an associative array containing `product` of
 * type Product and `count` of type int. The `count` entry represents how
 * many times the associated product is in the shopping basket.
 * Data structure: [`productId` => ["product" => :Product, "count" => :int]]
 */
class ShoppingBasketManager
{
    private const API_WORKFLOW_ID = 768;

    public static function addProductToBasket(Product $product)
    {
        $basket = SessionManager::getShopSession()->getShoppingBasket();

        if (!isset($basket[$product->getId()])) {
            $basket[$product->getId()] = ["product" => $product, "count" => 1];
        } else {
            $basket[$product->getId()]["count"]++;
        }
        SessionManager::getShopSession()->setShoppingBasket($basket);

        return json_encode($basket);
    }

    public static function removeProductFromBasket(Product $product)
    {
        $basket = SessionManager::getShopSession()->getShoppingBasket();

        if (!isset($basket[$product->getId()])) return;

        // Remove product entry or decrement counter
        if ($basket[$product->getId()]["count"] <= 1) {
            unset($basket[$product->getId()]);
        } else {
            $basket[$product->getId()]["count"]--;
        }
        SessionManager::getShopSession()->setShoppingBasket($basket);

        return json_encode($basket);
    }

    public static function clearBasket()
    {
        SessionManager::getShopSession()->clear();
    }

    /**
     * Create invoice on Congressus and send it.
     */
    public static function purchaseBasket()
    {
        $congressusClient = createCongressusClient();

        $saleInvoice = $congressusClient->createSaleInvoice(
            items: self::createSaleInvoiceItems(),
            member_id: RfidAuthenticator::getAuthenticatedMember()->getId(),
            invoice_workflow_id: self::API_WORKFLOW_ID,
        );

        $congressusClient->sendASaleInvoice($saleInvoice->getId());

        self::clearBasket();
    }

    private static function createSaleInvoiceItems(): array
    {
        $saleInvoiceItems = array();
        foreach (self::getShoppingBasket() as $entry) {
            $saleInvoiceItems[] = [
                "product_offer_id" => $entry["product"]->getProductOfferId(),
                "quantity" => $entry["count"]
            ];
        }
        return $saleInvoiceItems;
    }

    /**
     * @return  array<int, array>
     */
    public static function getShoppingBasket(): array
    {
        return SessionManager::getShopSession()->getShoppingBasket();
    }

    public static function isBasketEmpty(): bool
    {
        return empty(SessionManager::getShopSession()->getShoppingBasket());
    }

    public static function getBasketTotalPrice(): float
    {
        $totalPrice = 0;
        foreach (self::getShoppingBasket() as $entry) {
            $totalPrice += $entry["product"]->getPrice() * $entry["count"];
        }
        return floatval($totalPrice);
    }
}
