<?php

namespace Compucie\Touchscreen\ShopPage\Controller;

use Compucie\Database\Sale\SaleDatabaseManager;

class PurchaseLogger extends SaleDatabaseManager
{
    public function log(array $purchase): void
    {
        $purchaseId = $this->insertPurchase();

        foreach ($purchase as $productId => $purchaseItem) {
            /** @var \Compucie\Congressus\Model\Product $product */
            $product = $purchaseItem['product'];
            $quantity = $purchaseItem['count'];
            $this->insertPurchaseItem($purchaseId, $productId, $quantity, $product->getName(), $product->getPrice());
        }
    }
}
