<?php

namespace Compucie\Touchscreen\ShopPage\Controller;

use Compucie\Congressus\Model\Product;
use Compucie\Congressus\ModelProcessor;
use Compucie\Touchscreen\ShopPage\View\ShopPage;

use function Compucie\Touchscreen\env;

class ApiHandler extends \Compucie\Touchscreen\Main\Controller\ApiHandler
{
    protected function route(): void
    {
        match (self::getUriPath()) {
            "/shop/product-list" => $this->showProductList(),
            "/shop/add-product-to-basket" => $this->addProductToBasket(),
            "/shop/remove-product-from-basket" => $this->removeProductFromBasket(),
            "/shop/clear-basket" => $this->clearBasket(),
            "/shop/purchase-basket" => $this->purchaseBasket(),
        };
    }

    private function showProductList(): void
    {
        try {
            $this->respond(200, ShopPage::productList($_GET['selectedFolderId']));
        } catch (\Throwable) {
            $this->respond(500);
        }
    }

    private function addProductToBasket(): void
    {
        try {
            ShoppingBasketManager::addProductToBasket(ModelProcessor::json_decode($_POST["product"], Product::class));
            $this->respond(200, ShopPage::shoppingBasket());
        } catch (\Throwable) {
            $this->respond(500);
        }
    }

    private function removeProductFromBasket(): void
    {
        try {
            ShoppingBasketManager::removeProductFromBasket(ModelProcessor::json_decode($_POST["product"], Product::class));
            $this->respond(200, ShopPage::shoppingBasket());
        } catch (\Throwable) {
            $this->respond(500);
        }
    }

    private function clearBasket(): void
    {
        try {
            ShoppingBasketManager::clearBasket();
            $this->respond(200, ShopPage::shoppingBasket());
        } catch (\Throwable) {
            $this->respond(500);
        }
    }

    private function purchaseBasket(): void
    {
        try {
            $purchaseLogger = new PurchaseLogger(env('sale-database'));
            $purchaseLogger->log(ShoppingBasketManager::getShoppingBasket());
            ShoppingBasketManager::purchaseBasket();
            $this->respond(200);
        } catch (\Throwable) {
            $this->respond(500);
        }
    }
}
