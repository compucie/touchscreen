<?php

namespace Compucie\Touchscreen\ShopPage\View;

use Compucie\Congressus\Model\Product;
use Compucie\Congressus\Model\ProductFolder;
use Compucie\Congressus\ModelProcessor;

use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\View\Page;
use Compucie\Touchscreen\ShopPage\Controller\ShoppingBasketManager;
use Compucie\Touchscreen\Sidebar\View\Sidebar;

use function Compucie\Touchscreen\createCongressusClient;
use function Compucie\Touchscreen\env;

class ShopPage extends Page
{
    public static function generatePage(): string
    {
        $slugs = env("store-product-folder-slugs");
        $productFolders = createCongressusClient()->retrieveProductFoldersBySlug(...$slugs);
        $selectedFolderId = SessionManager::getShopSession()->getSelectedFolderId();
        $selectedFolderId = is_null($selectedFolderId) ? $productFolders[$slugs[0]]->getId() : $selectedFolderId;

        // HTML
        $titleBar = self::titleBar("Shop");
        $categoryMenu = self::categoryMenu($productFolders, $selectedFolderId);
        $productList = self::productList($selectedFolderId);
        $shoppingBasket = self::shoppingBasket();
        $sidebar = Sidebar::generateSidebar();

        return <<<HTML
            <main id="MainContent">
                <div id="EventArea" style="height: 100%">
                    $titleBar
                    $categoryMenu
                    $productList
                    $shoppingBasket
                </div>
            </main>

            $sidebar
        HTML;
    }


    // CATEGORY BUTTONS

    /**
     * @param   ProductFolder[]     $productFolders
     */
    private static function categoryMenu(array $productFolders, int $selectedProductFolderId): string
    {
        $categoryButtons = array();
        foreach ($productFolders as $productFolder) {
            $isActive = $productFolder->getId() == $selectedProductFolderId;
            $categoryButtons[] = self::categoryButton($productFolder, $isActive, count($productFolders));
        }
        $categoryButtons = implode($categoryButtons);

        return <<<HTML
            <div class="shop categoryMenuContainer">
                <div class="btn-group btn-group-toggle shop categoryMenu" data-toggle="buttons">
                    $categoryButtons
                </div>
            </div>
        HTML;
    }

    private static function categoryButton(ProductFolder $productFolder, bool $isActive, int $totalButtonCount)
    {
        $activeString = $isActive ? "active" : "";
        return <<<HTML
            <label class="btn shop categoryButton $activeString" onclick="showProductList({$productFolder->getId()})" >
                <input type="radio" name="options" autocomplete="off" checked> {$productFolder->getName()}
            </label>
        HTML;
    }


    // PRODUCT LIST

    public static function productList(int $productFolderId): string
    {
        $products = createCongressusClient()->listProducts(
            limit: null,
            folder_id: $productFolderId,
            order: "name"
        );

        $productCards = array();
        foreach ($products as $product) {
            $productCards[] = self::productCard($product);
        }
        $productList = implode($productCards);

        return <<<HTML
            <div id="EventsWrapper" class="shop" style="width: 70%; float: left;">
                $productList
            </div>
        HTML;
    }

    private static function productCard(Product $product)
    {
        $formattedPrice = self::formatCurrency($product->getPrice());
        try {
            $imageSource = self::findFirstImageUrl($product);
        } catch (NoImageFoundException) {
            $imageSource = "";
        }

        $productSerialized = htmlspecialchars(ModelProcessor::json_encode($product));

        return <<<HTML
            <div class="EventsBox" style="width: calc(100%/3); padding-top: calc(100%/3);">
                <div class="insideBox">

                    <!-- PRODUCT NAME -->
                    <div class="titleBox productCard" style="style: table; padding: 5px;">
                        {$product->getName()}
                    </div>

                    <!-- PRODUCT IMAGE -->
                    <div class="EventPicture " onclick='addProductToBasket({$productSerialized});'>
                        <img src="{$imageSource}" class="img product"/>
                    </div>

                    <!-- PRODUCT PRICE AND ADD/REMOVE BUTTONS -->
                    <div class="PriceArea">

                        <!-- REMOVE BUTTON -->
                        <div style="width: 25%; height: 100%; float: left;">
                            <button class="btn btn-block plusminus" onclick='removeProductFromBasket({$productSerialized});'>
                                -
                            </button>
                        </div>

                        <!-- PRICE -->
                        <div style="width: 50%; height: 100%; display: inline-block; text-align: center">
                            <h3>
                                {$formattedPrice}
                            </h3>
                        </div>

                        <!-- ADD BUTTON -->
                        <div style="width: 25%; height: 100%; float: right">
                            <button class="btn btn-block plusminus" onclick='addProductToBasket({$productSerialized});'>
                                +
                            </button>
                        </div>

                        <script>
                            $("button.btn.btn-block.plusminus").click(function() {
                                $("button.btn.btn-block.plusminus").blur();
                        })
                        </script>
                    </div>
                </div>
            </div>
        HTML;
    }


    // SHOPPING BASKET

    public static function shoppingBasket()
    {
        $shoppingBasketContent = self::shoppingBasketContent();
        $shoppingBasketControls = self::shoppingBasketControls(ShoppingBasketManager::isBasketEmpty());

        return <<<HTML
            <div id="shoppingBasketContainer">
                <div id="shoppingBasket" style="overflow-y: auto">
                    <h1 class="h1 shoppingBasket">Shopping basket</h1>
                    $shoppingBasketContent
                </div>

                <div class="shopButtonContainer">
                    $shoppingBasketControls
                </div>
            </div>
        HTML;
    }

    private static function shoppingBasketContent()
    {
        if (ShoppingBasketManager::isBasketEmpty()) {
            return <<<HTML
                <p style="color: gray">
                    Your shopping basket is empty
                </p>
            HTML;
        }

        $sumTotalPrice = self::formatCurrency(ShoppingBasketManager::getBasketTotalPrice());
        $shoppingBasketProducts = self::shoppingBasketProducts();

        return <<<HTML
            <table class="table header-fixed" style="width: 100%; height: 75%">
                <!-- COLUMN NAMES -->
                <thead>
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Count</th>
                        <th scope="col">Total</th>
                    </tr>
                </thead>

                <!-- PRODUCTS IN BASKET -->
                <tbody>
                    $shoppingBasketProducts
                </tbody>

                <!-- SUM TOTAL PRICE -->
                <thead>
                    <tr>
                        <th scope="col">Sum total:</th>
                        <th scope="col">&nbsp;</th>
                        <th scope="col">{$sumTotalPrice}</th>
                    </tr>
                </thead>
            </table>
        HTML;
    }

    private static function shoppingBasketProducts(): string
    {
        $shoppingBasketProducts = array();
        foreach (ShoppingBasketManager::getShoppingBasket() as $entry) {
            $combinedPrice = self::formatCurrency($entry['count'] * $entry['product']->getPrice());
            $shoppingBasketProducts[] = <<<HTML
                <tr>
                    <td>{$entry['product']->getName()}</td>
                    <td>{$entry['count']}</td>
                    <td>{$combinedPrice}</td>
                </tr>
            HTML;
        }
        return implode($shoppingBasketProducts);
    }

    private static function shoppingBasketControls(bool $isBasketEmpty): string
    {
        if ($isBasketEmpty) {
            return <<<HTML
                <button class="btn btn-secondary disabled btn-block" style="height: 42.5%; margin-bottom: 5%; font-size: 20pt;">Empty</button>
                <button class="btn btn-secondary disabled btn-block" style="height: 42.5%; font-size: 20pt;">Purchase</button>
            HTML;
        }

        return <<<HTML
            <button class="btn btn-block" onclick='clearBasket()' style="height: 42.5%; margin-bottom: 5%; font-size: 20pt;">Empty</button>
            <button class="btn btn-block" onclick='purchaseBasket()' style="height: 42.5%; font-size: 20pt;">Purchase</button>
        HTML;
    }


    // UTILS

    private static function findFirstImageUrl(Product $product): string
    {
        if (empty($product->getMedia())) {
            throw new NoImageFoundException;
        }

        foreach ($product->getMedia() as $media) {
            if ($media->getIsImage()) {
                return $media->getUrl();
            }
        }

        throw new NoImageFoundException;
    }
}


// EXCEPTIONS

class NoImageFoundException extends \Exception
{
}
