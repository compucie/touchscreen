<?php

namespace Compucie\Touchscreen;

use Compucie\Congressus\ExtendedClient;
use DateTime;
use DateTimeZone;

function env(string $key): string|array
{
    $env = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/.env", true);
    $keys = explode(".", $key);
    foreach ($keys as $k) {
        $env = $env[$k];
    }
    return $env;
}

function createCongressusClient(): ExtendedClient
{
    date_default_timezone_set("UTC"); // API responds with UTC times
    return new ExtendedClient(env("congressus.token"));
}

function convertTimeZone(DateTime $dateTime): DateTime
{
    $timeZone = new DateTimeZone(env("datetime.timezone"));
    return $dateTime->setTimezone($timeZone);
}

function puzzleServiceGet(string $path)
{
    $puzzleServiceLocation = env("puzzle.service-location");
    $ch = curl_init("$puzzleServiceLocation/$path");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}
