<?php

namespace Compucie\Touchscreen\EventPage\Model;

use Compucie\Congressus\Model\Event;

class EventPricing
{
    private bool $isSignupEnabled;
    private ?bool $hasSinglePrice = null;
    private ?float $lowestPrice = null;
    private ?float $highestPrice = null;

    public function __construct(Event $event)
    {
        $ticketTypes = $event->getTicketTypes();
        $this->isSignupEnabled = count($ticketTypes) > 0;

        if (!$this->isSignupEnabled) return;

        foreach ($ticketTypes as $ticketType) {
            $price = $ticketType->getPrice() ?? 0.0;
            if ($price < $this->lowestPrice || is_null($this->lowestPrice)) $this->lowestPrice = $price;
            if ($price > $this->highestPrice || is_null($this->highestPrice)) $this->highestPrice = $price;
        }

        $this->hasSinglePrice = ($this->lowestPrice == $this->highestPrice);
    }

    public function isSignupEnabled(): bool
    {
        return $this->isSignupEnabled;
    }

    public function hasSinglePrice(): ?bool
    {
        return $this->hasSinglePrice;
    }

    public function getLowestPrice(): ?float
    {
        return $this->lowestPrice;
    }

    public function getHighestPrice(): ?float
    {
        return $this->highestPrice;
    }
}
