<?php

namespace Compucie\Touchscreen\EventPage\View;

use Compucie\Congressus\Model\Event;
use Compucie\Congressus\Model\EventParticipationBuilder;
use Compucie\Congressus\Model\EventParticipationBuilderTicket;
use Compucie\Congressus\Model\EventTicketType;
use Compucie\Congressus\ModelProcessor;
use Compucie\Congressus\NoImageFoundException;
use Compucie\Touchscreen\Authentication\Controller\RfidAuthenticator;
use Compucie\Touchscreen\Authentication\Model\AuthenticatedMember;
use Compucie\Touchscreen\EventPage\Model\EventPricing;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\View\Page;
use Compucie\Touchscreen\Sidebar\View\Sidebar;
use DateTime;

use function Compucie\Touchscreen\convertTimeZone;
use function Compucie\Touchscreen\createCongressusClient;

class EventPage extends Page
{
    public static function generatePage(): string
    {
        $eventId = SessionManager::getMainSession()->getEventId();
        $event = createCongressusClient()->retrieveEvent($eventId);

        $titleBar = self::titleBar($event->getName());
        $imageUrl = self::getImageUrl($event);
        $start = convertTimeZone($event->getStart());
        $priceElement = self::priceElement($event);
        $participantsElement = self::participantElement($event);
        $subscribeButton = self::signupButton($event);
        $sidebar = Sidebar::generateSidebar();

        return <<<HTML
            <main id="MainContent">

                $titleBar

                <div id="EventSummary">

                    <!-- EVENT PICTURE -->
                    <div id="EventPicture" style="position: relative;">
                        <img src="{$imageUrl}" class="contain" />
                    </div>

                    <!-- EVENT DETAILS -->
                    <div id="EventDetails">
                        <h2 id="EventName">
                            {$event->getName()}
                        </h2>
                        <br>
                        <p>
                            Starts at: <span class="eventDetails">
                                {$start->format('H:i')} &nbsp {$start->format('j F')}</span> <br>
                            Location: <span class="eventDetails">{$event->getLocation()}</span> <br>
                            $priceElement
                            $participantsElement
                        </p>
                        <br>
                        {$subscribeButton}
                    </div>
                </div>

                <div id="EventDescription">
                    {$event->getDescription()}
                    <script>
                        // Disable any links in the event description to prevent breaking out of kiosk mode
                        $(() => {
                            $('a').removeAttr('href');
                        });
                    </script>
                </div>
            </main>

            $sidebar
        HTML;
    }

    private static function getImageUrl(Event $event): string
    {
        try {
            return ModelProcessor::getFirstImage($event->getMedia())->getUrl();
        } catch (NoImageFoundException) {
            return "";
        }
    }

    private static function priceElement(Event $event): string
    {
        if (!self::areTicketsSold($event)) return "";
        $eventPricing = new EventPricing($event);

        if ($eventPricing->hasSinglePrice()) {
            $price = $eventPricing->getHighestPrice();
            $priceText = ($price == 0) ? "FREE!" : self::formatCurrency($price);
        } else {
            $lowest = self::formatCurrency($eventPricing->getLowestPrice());
            $highest = self::formatCurrency($eventPricing->getHighestPrice());
            $priceText = "$lowest - $highest";
        }

        return <<<HTML
            Price: <span class="eventDetails"> $priceText </span> <br>
        HTML;
    }

    private static function participantElement(Event $event): string
    {
        if (!self::areTicketsSold($event)) return "";

        $capacity = $event->getNumTickets();
        $capacityString = is_null($capacity) ? "" : "<span style=\"color: gray;\"> / $capacity</span>";

        return <<<HTML
            Participants: <span class="eventDetails">{$event->getNumTicketsSold()}{$capacityString}</span> <br>
        HTML;
    }

    private static function signupButton(Event $event): string
    {
        // No tickets in Congressus -> no sign up
        if (!self::areTicketsSold($event)) {
            return self::popupButton(
                "btn btn-block btn-secondary disabled",
                "No sign-up",
                "Either this event doesn't require sign-up, or you can't sign up just yet, or you have to sign up outside of Newton.",
                10000,
                "info",
            );
        }

        // Ticketed events may require the buyer to fill in the name and email address per attendee on each ticket,
        // which is not possible at the touchscreen. It is possible to allow buying tickets at the touchscreen
        // when participation_information_collection_type is set to "order" and the touchscreen user is able
        // to select the amount of tickets they want.
        if ($event->getParticipationMode() == "ticketing") {
            return self::popupButton(
                "btn btn-block btn-secondary disabled",
                "Sign up on website",
                "Signing up for ticketed events via the touchscreen is currently not possible.",
                5000,
                "info",
            );
        }

        // Only allow touchscreen sign-up for events that are paid using direct debit. Tickets on invoice lead
        // to defaulters and payment through iDeal is not possible through the touchscreen (and expensive).
        if (!$event->getParticipationPaymentDirectDebit()) {
            return self::popupButton(
                "btn btn-block btn-secondary disabled",
                "Ticket sale not supported",
                "The tickets on sale are not payable via direct debit and the sale of these tickets is therefore not supported on the touchscreen",
                3000,
                "info",
            );
        }

        // Sign-up allowed -> check if member is logged in
        if (!RfidAuthenticator::isAuthenticated()) {
            return self::popupButton(
                "btn btn-block btn-secondary disabled",
                "Log in to sign up",
                "Please log in to sign up",
                2500,
                "info",
            );
        }

        // Member logged in -> check if member is already participating
        $member = RfidAuthenticator::getAuthenticatedMember();
        $participations = createCongressusClient()->listEventParticipations(
            limit: null,
            obj_id: $event->getId(),
            member_id: $member->getId(),
            status: ["approved", "waiting list", "payment pending"],
        );
        $isPartipant = count($participations) > 0;

        // Participating but sign-up type is 'ticketing' -> no cancellation at touchscreen
        if ($isPartipant && $event->getParticipationMode() == "ticketing") {
            return self::disabledCancelSignupButton("Please cancel your sign-up for this event on the Newton website.");
        }

        // Participating and sign-up type is 'single' -> check if ticket can (still) be cancelled
        // Remark: there does not appear to be a way in the API to link a participation to a ticket type. Therefore,
        // it is not possible to know what the member's ticket type is, except when there is only a single ticket type.
        $ticketTypes = $event->getTicketTypes();
        if ($isPartipant && count($ticketTypes) == 1) {
            $cancelTo = $ticketTypes[0]->getCancelTo();

            // Check if cancelling is disabled
            if (is_null($cancelTo)) return self::disabledCancelSignupButton("Cancelling is disabled for this event.");

            // Check if cancelling is too late
            $now = new DateTime;
            if ($now > $cancelTo) return self::disabledCancelSignupButton("You cannot cancel anymore.");

            // Allow cancelling
            return self::cancelSignupButton($event, $member);
        }

        // Participating and sign-up type is 'single' but multiple tickets are available -> check if ticket can (still) be cancelled
        // Remark: there does not appear to be a way in the API to link a participation to a ticket type. Therefore,
        // it is not possible to know what the member's ticket type is. To circumvent this, we check if all ticket types
        // for the event are still cancellable.
        if ($isPartipant && !self::areAllTicketTypesCancellable($event)) {
            return self::disabledCancelSignupButton("Please cancel your sign-up for this event on the Newton website.");
        }

        // Not participating -> check if ticket type needs not be chosen
        $allowInstantSignup = (count($event->getTicketTypes()) == 1) && ($event->getParticipationMode() == "single");
        if ($allowInstantSignup) {
            return self::instantSignupButton($event);
        }

        // Ticket type needs to be chosen
        return self::ticketSelectionButton($event);
    }

    private static function areTicketsSold(Event $event): bool
    {
        return count($event->getTicketTypes()) > 0;
    }

    private static function areAllTicketTypesCancellable(Event $event): bool
    {
        foreach ($event->getTicketTypes() as $ticketType) {
            if (!self::isTicketTypeCancellable($ticketType)) return false;
        }
        return true;
    }

    private static function isTicketTypeCancellable(EventTicketType $ticketType): bool
    {
        $now = new DateTime;
        $cancelTo = $ticketType->getCancelTo();
        return (is_null($cancelTo) || ($now > $cancelTo));
    }

    /**
     * Button that signs up the member for the first ticket type. Intended to be
     * used when "sign up" is the sign-up type and there is only 1 ticket type.
     */
    private static function instantSignupButton(Event $event): string
    {
        $member = RfidAuthenticator::getAuthenticatedMember();

        $ticket = new EventParticipationBuilderTicket;
        $ticket->setTicketTypeId($event->getTicketTypes()[0]->getId());

        $eventParticipation = new EventParticipationBuilder;
        $eventParticipation->setTickets([$ticket]);
        $eventParticipation->setMemberId($member->getId());
        $eventParticipation->setInvoiceAddressee($member->getFullName());
        $eventParticipation->setInvoiceEmail($member->getEmail());
        $eventParticipation->setInvoiceAddress($member->getAddress());

        $eventParticipation = ModelProcessor::json_encode($eventParticipation);

        return <<<HTML
            <div class="subscribeButtonDetailContainer">
                <button id="SignupButton" class="btn btn-block" onclick='signupForEvent({$event->getId()}, {$eventParticipation})'>
                    Sign-up
                </button>
            </div>
        HTML;
    }

    private static function cancelSignupButton(Event $event, AuthenticatedMember $member): string
    {
        return <<<HTML
            <div class="subscribeButtonDetailContainer">
                <button id="SignupButton" class="btn btn-block" onclick="cancelSignupForEvent('{$event->getId()}', '{$member->getId()}')">
                    Cancel sign-up
                </button>
                <p style="text-align: center; font-size: large;">(No confirmation!)</p>
            </div>
        HTML;
    }

    private static function disabledCancelSignupButton(string $popupMessage): string
    {
        return self::popupButton(
            "btn btn-block btn-secondary disabled",
            "Cancel sign-up",
            $popupMessage,
            5000,
            "info",
        );
    }

    private static function ticketSelectionButton(Event $event): string
    {
        $dropdownButtons = "";
        foreach ($event->getTicketTypes() as $ticketType) {
            if ($ticketType->getAvailabilityStatus() == "available") {
                $formattedPrice = parent::formatCurrency($ticketType->getPrice());
                $text = "{$ticketType->getName()} for {$formattedPrice}";
                $dropdownButtons .= self::dropdownButton("dropdown-item", $text, $event, $ticketType);
            } else {
                $text = "{$ticketType->getName()} [{$ticketType->getAvailabilityStatus()}]";
                $dropdownButtons .= self::popupButton(
                    "dropdown-item disabled",
                    $text,
                    "It is not possible to buy this ticket type on the touchscreen",
                    3000,
                    "error",
                );
            }
        }

        return <<<HTML
            <div class="subscribeButtonDetailContainer">
                <div class="dropdown">
                    <!-- SUBSCRIBE BUTTON -->
                    <button id="SignupButton" class="btn btn-block btn-primary dropdown-toggle" style="height: 100%;  font-size: 20pt;" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select ticket
                    </button>

                    <!-- DROPDOWN MENU -->
                    <div class="dropdown-menu" style="width: 100%;" aria-labelledby="dropdownMenuButton">
                        {$dropdownButtons}
                    </div>
                </div>
            </div>
        HTML;
    }

    private static function dropdownButton(string $class, string $text, Event $event, EventTicketType $ticketType): string
    {
        if (!RfidAuthenticator::isAuthenticated()) {
            return <<<HTML
                <button class="{$class}">
                    Please log in to sign up.
                </button>
            HTML;
        }

        $member = RfidAuthenticator::getAuthenticatedMember();

        $ticket = new EventParticipationBuilderTicket;
        $ticket->setTicketTypeId($ticketType->getId());

        $eventParticipation = new EventParticipationBuilder;
        $eventParticipation->setTickets([$ticket]);
        $eventParticipation->setMemberId($member->getId());
        $eventParticipation->setInvoiceAddressee($member->getFullName());
        $eventParticipation->setInvoiceEmail($member->getEmail());
        $eventParticipation->setInvoiceAddress($member->getAddress());

        $eventParticipation = ModelProcessor::json_encode($eventParticipation);

        return <<<HTML
            <button class="{$class}" onclick='signupForEvent({$event->getId()}, {$eventParticipation})'>
                {$text}
            </button>
        HTML;
    }

    private static function popupButton(string $class, string $text, string $popupMessage, int $popupDuration, string $popupType): string
    {
        return <<<HTML
            <div class="subscribeButtonDetailContainer">
                <button id="PopupButton" class="{$class}" onclick="showPopup(`{$popupMessage}`, '{$popupDuration}', '{$popupType}')">
                    {$text}
                </button>
            </div>
        HTML;
    }
}
