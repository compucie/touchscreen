<?php

namespace Compucie\Touchscreen\EventPage\Controller;

class ApiHandler extends \Compucie\Touchscreen\Main\Controller\ApiHandler
{
    protected function route(): void
    {
        match ($_SERVER['REQUEST_URI']) {
            "/event/subscribe" => $this->subscribe(),
            "/event/unsubscribe" => $this->unsubscribe(),
        };
    }

    private function subscribe(): void
    {
        try {
            $this->getCongressusClient()->createEventParticipation($_POST['eventId'], ...$_POST['eventParticipation']);
            $this->respond(201);
        } catch (\Throwable $th) {
            $this->respond(500, $th->getMessage());
        }
    }

    private function unsubscribe(): void
    {

        try {
            $participations = $this->getCongressusClient()->listEventParticipations(
                limit: null,
                obj_id: $_POST['eventId'],
                member_id: $_POST['memberId'],
                status: ["approved", "waiting list", "payment pending"]
            );

            foreach ($participations as $participation) {
                $this->getCongressusClient()->unsubscribeParticipation($_POST['eventId'], $participation->getId());
            }

            $this->respond(201);
        } catch (\Throwable $th) {
            $this->respond(500, $th->getMessage());
        }
    }
}
