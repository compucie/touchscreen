<?php

namespace Compucie\Touchscreen\Puzzle\Model;

use Compucie\Congressus\ExtendedClient;

use function Compucie\Touchscreen\convertTimeZone;
use function Compucie\Touchscreen\createCongressusClient;

class UpcomingEvents
{
    private ExtendedClient $congressusClient;
    private array $events;

    public function __construct()
    {
        $this->congressusClient = createCongressusClient();
        $this->events = $this->congressusClient->listUpcomingEvents(8);
    }

    public function asColumn(int $offset, int  $column_line_height): string
    {
        $eventsColumn = <<<HTML
            <style>
                .date {
                    display: inline-block;
                    width: 2cm; 
                    text-align: right;
                    padding-right: 5%;
                }
            </style>
        HTML;

        $eventsSlice = array_slice($this->events, $offset, $column_line_height);
        foreach ($eventsSlice as $event) {
            $start = convertTimeZone($event->getStart())->format("j M");
            $eventsColumn .= <<<HTML
                <span class='date'>{$start}</span>{$event->getName()}<br>
            HTML;
        }

        return $eventsColumn;
    }
}
