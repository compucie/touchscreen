<?php

namespace Compucie\Touchscreen\Puzzle\View;

use Compucie\Touchscreen\Puzzle\Model\UpcomingEvents;

use function Compucie\Touchscreen\env;
use function Compucie\Touchscreen\puzzleServiceGet;

require_once("{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php");

?>
<!DOCTYPE html>
<html>

<?php
// Always use the server-side puzzle image
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Expires: Thu, 1 Jan 1970 00:00:00 GMT');
?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        @import url("https://fonts.cdnfonts.com/css/open-sans");

        @page {
            size: A4 landscape;
            margin: 0.5cm;
            /* no margin can cause the printer to clip the file */
        }

        :root {
            --page-width: 29.7cm;
            --page-height: 20.5cm;
            /* correct A4 height overflows to next page for some reason */

            --header-height: 2.6cm;
            --puzzle-height: calc(var(--page-height) - var(--header-height) - 0.3cm);
        }

        @media all {
            * {
                box-sizing: border-box;

                /* Newton corporate identity*/
                font-family: "Open Sans", sans-serif;
                font-size: large;
            }

            /* Only for debugging */
            body {
                background: rgb(204, 204, 204);
            }

            .page {
                background: white;
                width: var(--page-width);
                height: var(--page-height);
                margin: 0 auto;
                margin-bottom: 0.5cm;
                display: flex;
                flex-direction: column;
            }

            img {
                object-fit: contain;
                max-width: 100%;
                max-height: 100%;
                display: block;
                margin-left: auto;
                margin-right: auto;
                height: 100%;
            }

            .row {
                display: flex;
                flex-flow: row;
                column-gap: 1cm;
            }

            .column {
                padding-bottom: 10px;
                height: var(--header-height);
                /* setting height in row is more cumbersome to work with */
            }

            .column.events {
                flex: 1 1 0;

                /* add ellipses to line if the line is too long for the column */
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }

            .column.logo {
                flex: none;
                max-width: 5cm;
            }

            .puzzle {
                max-height: var(--puzzle-height);
            }
        }

        @media screen {
            * {
                display: none;
            }
        }
    </style>
</head>

<body>
    <div class="page">
        <div class="row">

            <!-- Event columns -->
            <?php
            define("COLUMN_LINE_HEIGHT", 4);
            $events = new UpcomingEvents;
            ?>

            <div id="events-left" class="column events">
                <?php echo $events->asColumn(0, COLUMN_LINE_HEIGHT); ?>
            </div>
            <div id="events-right" class="column events">
                <?php echo $events->asColumn(COLUMN_LINE_HEIGHT, COLUMN_LINE_HEIGHT); ?>
            </div>

            <!-- Logos -->
            <div class="column logo">
                <img src="<?php echo puzzleServiceGet("advertisement-location") ?>">
            </div>
            <div class="column logo">
                <img src="<?php echo env("puzzle.logo") ?>">
            </div>
        </div>

        <!-- Puzzle -->
        <div id="puzzle" class="puzzle">
            <img src="<?php echo $_GET["puzzleSrc"] ?>">
        </div>
    </div>
</body>

</html>