<?php

namespace Compucie\Touchscreen\Puzzle\View;

use function Compucie\Touchscreen\puzzleServiceGet;

class PuzzleMenu
{
    private const PUZZLE_ICONS_LOCATION = "/resources/puzzle";

    public static function puzzleMenu()
    {
        $puzzleLocations = self::getPuzzleLocations();

        if (empty($puzzleLocations)) {
            return <<<HTML
                    <div class="puzzleMenu">
                        <div style="height: 100%; display: flex; align-items: center; justify-content: center;">
                            <span style="font-size: 20pt; display: block; width: 80%; vertical-align: middle; text-align: center" class="align-middle">
                                Please ask the board to add puzzles to the folder
                            </span>
                        </div>
                    </div>
                HTML;
        }

        $puzzleButtons = "";
        foreach ($puzzleLocations as $puzzleLocation) {
            $puzzleButtons .= self::puzzleButton($puzzleLocation);
        }

        return <<<HTML
                <div class="puzzleMenu">
                    {$puzzleButtons}
                </div>
            HTML;
    }

    private static function puzzleButton(string $puzzleLocation): string
    {
        strtolower(preg_match("/[^\/]*(?=\.(png|jpg))/", $puzzleLocation, $match));
        $puzzleName = strtolower($match[0]);
        $puzzleNameHtml = ucfirst($puzzleName);

        $puzzleIconsLocation = self::PUZZLE_ICONS_LOCATION;
        $puzzleIconLocation = file_exists("{$_SERVER['DOCUMENT_ROOT']}/$puzzleIconsLocation/$puzzleName.svg") ? ("$puzzleIconsLocation/$puzzleName.svg") : ("$puzzleIconsLocation/default.svg");

        return <<<HTML
                <button class="btn puzzleButton border container" onclick="openPrintDialog('{$puzzleLocation}'); this.blur();">
                    <div class="row align-items-center justify-center" style="height: 100%;">
                        
                        <!-- Puzzle name -->
                        <div class="col-8 h-100" style="display: table">
                            <span style="font-size: 20pt; display: table-cell; height: 100%; width: 100%; vertical-align: middle;" class="align-middle">
                                {$puzzleNameHtml}
                            </span>
                        </div>
    
                        <!-- Icon -->
                        <div class="col-4 h-100">
                            <img src="{$puzzleIconLocation}" style="float: right; display: inline;  height: 95%; width: auto;">
                        </div>
    
                    </div>
                </button>
            HTML;
    }

    private static function getPuzzleLocations(): ?array
    {
        return json_decode(puzzleServiceGet("puzzle-locations"), associative: true);
    }
}
