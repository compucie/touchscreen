<?php

namespace Compucie\Touchscreen\Authentication\Model;

use Compucie\Touchscreen\Authentication\Model\AuthenticatedMember;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;
use Compucie\Touchscreen\Main\Model\Session;

class AuthenticationSession extends Session
{
    public const MEMBER = "member";
    public const LAST_ACTION = "lastAction";

    public function clear(): void
    {
        $this->setVars([
            self::MEMBER => null,
            self::LAST_ACTION => null,
        ]);
    }

    public function manage(): void
    {
        if ($this->isExpired()) {
            SessionManager::setPageType(PageType::HOME);
            $this->clear();
            echo <<<HTML
                <script>showPopup('You have been logged off due to inactivity', 2000, 'warning')</script>
            HTML;
            return;
        }

        if ($this->getMember() !== null) {
            $this->setLastAction(time());
        }
    }

    private function isExpired()
    {
        if (is_null($this->getMember()) || is_null($this->getLastAction())) {
            return false;
        }

        $inactivityThreshold = 4 * 60; // minutes * seconds
        return time() - $this->getLastAction() > $inactivityThreshold;
    }

    public function getMember(): ?AuthenticatedMember
    {
        return $this->get(self::MEMBER);

    }

    public function getLastAction(): ?int
    {
        return $this->get(self::LAST_ACTION);
    }

    public function setMember(AuthenticatedMember $member): void
    {
        $this->set(self::MEMBER, $member);
    }

    private function setLastAction(int $member): void
    {
        $this->set(self::LAST_ACTION, $member);
    }
}
