<?php

namespace Compucie\Touchscreen\Authentication\Model;

use Compucie\Congressus\Model\Member;
use Compucie\Congressus\Model\MemberWithCustomFields;
use Compucie\Congressus\ModelProcessor;

class AuthenticatedMember extends MemberWithCustomFields
{
    private bool $hasDutchBankAccount;
    private bool $hasValidSddMandate;
    private bool $hasOverdueInvoice;

    public static function createFromParentObject(Member|MemberWithCustomFields $member): AuthenticatedMember
    {
        return ModelProcessor::typecast($member, AuthenticatedMember::class);
    }

    public function mayUseShop(): bool
    {
        return $this->hasDutchBankAccount && $this->hasValidSddMandate && !$this->hasOverdueInvoice;
    }

    public function getFullName(): string
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }


    // GETTERS

    public function hasDutchBankAccount(): bool
    {
        return $this->hasDutchBankAccount;
    }

    public function hasValidSddMandate(): bool
    {
        return $this->hasValidSddMandate;
    }

    public function hasOverdueInvoice(): bool
    {
        return $this->hasOverdueInvoice;
    }


    // SETTERS

    public function setHasDutchBankAccount(bool $hasDutchBankAccount): void
    {
        $this->hasDutchBankAccount = $hasDutchBankAccount;
    }

    public function setHasValidSddMandate(bool $hasValidSddMandate): void
    {
        $this->hasValidSddMandate = $hasValidSddMandate;
    }

    public function setHasOverdueInvoice(bool $hasOverdueInvoice): void
    {
        $this->hasOverdueInvoice = $hasOverdueInvoice;
    }
}
