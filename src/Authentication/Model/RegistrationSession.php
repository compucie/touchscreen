<?php

namespace Compucie\Touchscreen\Authentication\Model;

use Compucie\Congressus\Model\MemberWithCustomFields;

use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;
use Compucie\Touchscreen\Main\Model\Session;

class RegistrationSession extends Session
{
    public const CARD = "card";
    public const MEMBER = "member";

    public function clear(): void
    {
        $this->setVars([
            self::CARD => null,
            self::MEMBER => null,
        ]);
    }

    public function manage(): void
    {
        if (SessionManager::getPageType() != PageType::CARD_REGISTRATION) {
            $this->clear();
        }
    }

    public function getCard(): ?string
    {
        return $this->get(self::CARD);
    }

    public function getMember(): ?MemberWithCustomFields
    {
        return $this->get(self::MEMBER);
    }

    public function setCard(string $card): void
    {
        $this->set(self::CARD, $card);
    }

    public function setMember(MemberWithCustomFields $member): void
    {
        $this->set(self::MEMBER, $member);
    }
}
