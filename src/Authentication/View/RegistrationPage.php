<?php

namespace Compucie\Touchscreen\Authentication\View;

use Compucie\Touchscreen\Authentication\Controller\RegistrationStage;
use Compucie\Touchscreen\Authentication\Controller\RfidAuthenticator;
use Compucie\Touchscreen\Authentication\Controller\UnknownRegistrationStage;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\View\Page;
use Compucie\Touchscreen\Sidebar\View\Sidebar;

class RegistrationPage extends Page
{
    public static function generatePage(): string
    {
        $titleBar = self::titleBar("Register card");
        $sidebar = Sidebar::generateSidebar();

        // I hate this with a passion
        $scanInstruction = "";
        $numpad = "";
        $searchInstruction = "";
        $emailConfirmation = "";
        switch (RfidAuthenticator::detectRegistrationStage()) {
            case RegistrationStage::SCAN_CARD:
                $scanCardCheckmark = self::checkmark("gray");
                $enterStudentIdCheckmark = self::checkmark("gray");
                $confirmEmailCheckmark = self::checkmark("gray");
                $scanInstruction = <<<HTML
                    <p>Put <b>any card</B> with an RFID tag on the scanner below the screen.</p>
                HTML;
                break;
            case RegistrationStage::ENTER_STUDENT_NUMBER:
                $scanCardCheckmark = self::checkmark("green");
                $enterStudentIdCheckmark = self::checkmark("gray");
                $confirmEmailCheckmark = self::checkmark("gray");
                $numpad = self::numpad();
                $searchInstruction = <<<HTML
                    <p>Enter your student number and press the search button!<p>
                HTML;
                break;
            case RegistrationStage::CONFIRM_EMAIL:
                $scanCardCheckmark = self::checkmark("green");
                $enterStudentIdCheckmark = self::checkmark("green");
                $confirmEmailCheckmark = self::checkmark("gray");
                $emailAddress = SessionManager::getRegistrationSession()->getMember()->getEmail();
                $emailConfirmation = self::emailConfirmationElement($emailAddress);
                break;
            default:
                throw new UnknownRegistrationStage;
        }

        return <<<HTML
            <main id="MainContent">

                $titleBar

                <div class="content registration">
                    <h1 style="color: black">Welcome to the touchscreen!</h1>
                    <h3 style="color: black">Follow these 3 steps to register:</h3>

                    <div class="row registration">

                        <!-- STEP: SCAN CARD -->
                        <div class="col">
                            <div class="row" style="width: 100%; text-align: center; vertical-align: center; display:inline-block;">
                                {$scanCardCheckmark}<br><br>
                                <div class="row" style="width: 100%; text-align: center; vertical-align: center; display:inline-block;">
                                    <h3>Scan a card</h3>
                                    $scanInstruction
                                </div>
                            </div>
                        </div>

                        <!-- STEP: ENTER STUDENT NUMBER -->
                        <div class="col" id="studentIDBox" style="width: 100%; text-align: center; vertical-align: center; display:inline-block;">
                            {$enterStudentIdCheckmark}<br><br>
                            <h3>Enter student number</h3>
                            $numpad
                            $searchInstruction
                        </div>

                        <!-- STEP: CONFIRM EMAIL -->
                        <div class="col" style="width: 100%; text-align: center; vertical-align: center; display:inline-block;">
                            {$confirmEmailCheckmark}<br><br>
                            <h3>Confirm email</h3>
                            $emailConfirmation
                        </div>
                    </div>
                </div>

                <!-- JS: NUMPAD CONTROL -->
                <script>
                    $(document).ready(function() {
                        $("button.numpad-Number").click(function() {
                            let input = $("#studentNumber").val();
                            if (input.length < $("#studentNumber").attr("maxlength")) {
                                $("#studentNumber").val(input + this.value);
                            }
                            $("button.numpad-Number").blur();
                        });
            
                        $("button.numpad-Backspace").click(function() {
                            var input = $("#studentNumber").val();
                            $("#studentNumber").val(input.slice(0, -1));
                        });
                    });
                </script>

            </main>
            
            $sidebar
            HTML;
    }

    private static function checkmark(string $color)
    {
        return <<<HTML
            <span class="fas fa-check-circle" style="color: {$color}; font-size: 72pt; text-align: center;"></span>
        HTML;
    }

    private static function numpad()
    {
        $numpadButtons[0] = self::numpadButtons(7, 8, 9);
        $numpadButtons[1] = self::numpadButtons(4, 5, 6);
        $numpadButtons[2] = self::numpadButtons(1, 2, 3);
        $numpadButtons[3] = self::numpadButtons(0);
        $backspaceButton = self::backspaceButton();
        $searchButton = self::searchButton();

        return <<<HTML
            <div style="display: inline-block;">
                <div class="registration numberField">
                    <div class="input-group-text">s</div>
                    <input name="studentNumber" maxLength="7" type="text" class="form-control" id="studentNumber" value="" placeholder="1234567">
                </div>

                <table id="numpad" class="table" style="display:none">
                    <tr>
                        {$numpadButtons[0]}
                    </tr>
                    <tr>
                        {$numpadButtons[1]}
                    </tr>
                    <tr>
                        {$numpadButtons[2]}
                    </tr>
                    <tr>
                        $backspaceButton
                        {$numpadButtons[3]}
                        $searchButton
                    </tr>
                </table>
            </div>

            <script>
                $(document).ready(function() {
                    $('#numpad').fadeIn('fast');
                    $("#searchStudentNumber").click(function() {
                        searchMemberToRegister("s" + $("#studentNumber").val());
                    });
                });
            </script>
        HTML;
    }

    private static function numpadButtons(int ...$numbers)
    {
        $buttonsHtml = "";
        foreach ($numbers as $number) {
            $buttonsHtml .= <<<HTML
                <td> <button value="{$number}" class="registration btn numpad-Number btn-lg btn-block">{$number}</button> </td>
            HTML;
        }
        return $buttonsHtml;
    }

    private static function backspaceButton(): string
    {
        return <<<HTML
            <td> <button value="" class="registration btn numpad-Backspace btn-lg btn-block"><i class="fas fa-arrow-alt-circle-left"></i></button> </td>
        HTML;
    }

    private static function searchButton(): string
    {
        return <<<HTML
            <td> <button value="" class="registration btn btn-lg btn-block" id="searchStudentNumber"><i class="fas fa-search fa-lg"></i></button> </td>
        HTML;
    }

    private static function emailConfirmationElement(string $emailAddress)
    {
        // Censor the email address
        $CENSORLESS_LENGTH = 2; // part of the address that shouldn't be censored
        $localPart = explode("@", $emailAddress)[0];
        $censorString = (strlen($localPart) >= $CENSORLESS_LENGTH) ? (str_repeat("*", strlen($localPart) - $CENSORLESS_LENGTH)) : "";
        $censoredLocalPart = substr_replace($localPart, $censorString, $CENSORLESS_LENGTH);
        $censoredEmailAddress = str_replace($localPart, $censoredLocalPart, $emailAddress);

        // Return the HTML element containing the censored email address
        return <<<HTML
            <p>The email address that is linked to this student number is: <br><br>
                <i>{$censoredEmailAddress}</i><br><br>
                We will email you a link to activate your card.
            </p>
            <button class="btn btn-block" onclick='sendRegistrationEmail()' style="height: 100px; font-size: 20pt;">
                Send email
            </button>
        HTML;
    }
}
