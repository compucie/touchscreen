<?php

namespace Compucie\Touchscreen\Authentication\View;

class RegistrationAuthorizationPage
{
    public static function page(string $title, string $text): string
    {
        return <<<HTML
            <!DOCTYPE html>
            <html lang="en">

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" outside="width=device-width, initial-scale=1.0">
                <title>Registration result</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
                    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
                <style>
                    @import url("https://fonts.cdnfonts.com/css/open-sans");

                    @media screen {
                        html {
                            height: 100%;
                            size: landscape;
                            background-color: #c1e1bd;
                        }

                        .outside {
                            background-color: #489a2b;
                            width: 100%;

                            /* vertical centering */
                            position: absolute;
                            top: 50%;
                            transform: translate(0, -50%);

                            /* Padding outside border */
                            padding: 1.5%;
                        }

                        .inside {
                            /* Newton website font style*/
                            text-align: center;
                            font-family: "Open Sans", sans-serif;
                            font-size: large;
                            font-weight: 400;
                            color: white;

                            /* border */
                            border: solid 5px;
                            padding-top: 5%;
                            padding-bottom: 3.5%;
                        }
                    }
                </style>
            </head>

            <body>
                <div class="outside">
                    <div class="inside">
                        <p style="font-size: 40pt; font-weight: bold;">$title</p>
                        <p style="font-size: 28pt;">$text</p>
                    </div>
                </div>
            </body>

            </html>
        HTML;
    }
}
