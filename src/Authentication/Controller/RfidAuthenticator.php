<?php

namespace Compucie\Touchscreen\Authentication\Controller;

use PHPMailer\PHPMailer\PHPMailer;
use Compucie\Congressus\Exception\UserNotFoundException;
use Compucie\Congressus\Model\Member;
use Compucie\Congressus\Model\MembershipStatus;
use Compucie\Congressus\Model\MemberWithCustomFields;
use Compucie\Database\Member\MemberDatabaseManager;

use Compucie\Touchscreen\Authentication\Model\AuthenticatedMember;
use Compucie\Touchscreen\Main\Controller\SessionManager;

use function Compucie\Touchscreen\createCongressusClient;
use function Compucie\Touchscreen\env;

class RfidAuthenticator
{
    /***************************************************************
     * REGISTRATION
     ***************************************************************/

    public static function detectRegistrationStage(): RegistrationStage
    {
        $session = SessionManager::getRegistrationSession();
        if (is_null($session->getCard())) {
            return RegistrationStage::SCAN_CARD;
        } elseif (is_null($session->getMember())) {
            return RegistrationStage::ENTER_STUDENT_NUMBER;
        } else {
            return RegistrationStage::CONFIRM_EMAIL;
        }
    }

    /**
     * @throws  CardAlreadyRegisteredException
     */
    public static function registerCard(string $card): void
    {
        self::logout();
        $dbm = new MemberDatabaseManager(env("member-database"));
        if ($dbm->isRfidCardRegistered($card)) {
            throw new CardAlreadyRegisteredException;
        }
        SessionManager::getRegistrationSession()->setCard($card);
    }

    /**
     * @throws  UserNotFoundException
     * @throws  MembershipStatusNotAllowedException
     */
    public static function searchMemberToRegister(string $username): void
    {
        $member = createCongressusClient()->retrieveMemberByUsername($username);

        if (self::isMembershipStatusAllowed($member->getStatus())) {
            SessionManager::getRegistrationSession()->setMember($member);
        } else {
            throw new MembershipStatusNotAllowedException;
        }
    }

    /**
     * @throws  EmailingFailedException
     */
    public static function sendActivationEmail(): void
    {
        $session = SessionManager::getRegistrationSession();
        $card = $session->getCard();
        $member = $session->getMember();

        if (is_null($member) || is_null($card) || !ctype_alnum($card)) throw new EmailingFailedException;

        $secretQuery = self::generateCardSecretQuery($card, $member);
        $activationLink = "{$_SERVER['HTTP_HOST']}/registration/activate-card?$secretQuery";

        // Email content
        $emailSubject = htmlspecialchars("Activation card touchscreen");
        $emailBodyLocation = env("rfid-email.activation-email");
        $emailBody = file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/$emailBodyLocation");
        $emailBody = str_replace("{{ Name }}", $member->getFirstName(), $emailBody);
        $emailBody = str_replace("{{ Link }}", $activationLink, $emailBody);

        //ALLOW insecure apps needs to be enabled at google. TODO: It would be better to add 2fa to the account and app password
        $emailClient = new PHPMailer(exceptions: true);
        $emailClient->setFrom(env("rfid-email.sender-address"), env("rfid-email.sender-name"));
        $emailClient->clearAllRecipients();
        $emailClient->addAddress($member->getEmail(), $member->getFirstName());
        $emailClient->isSMTP();
        $emailClient->Host = env("rfid-email.host");
        $emailClient->SMTPAuth = true;
        $emailClient->Username = env("rfid-email.username");
        $emailClient->Password = env("rfid-email.password");
        $emailClient->SMTPSecure = 'tls';
        $emailClient->Port = 587; // SMTPS
        $emailClient->Subject = $emailSubject;
        $emailClient->IsHTML(true);
        $emailClient->Body = $emailBody;
        $emailClient->send();
        $emailClient->clearAllRecipients();

        $session->clear();
    }

    private static function generateCardSecretQuery(string $cardId, Member|MemberWithCustomFields $member): string
    {
        $memberId = $member->getId();
        $time = time();
        $secret = password_hash(self::generateCardSecret($memberId, $cardId, $time), PASSWORD_BCRYPT);
        return http_build_query(["memberId" => $memberId, "cardId" => $cardId, "time" => $time, "secret" => $secret]);
    }

    private static function generateCardSecret(string $memberId, string $cardId, string $time): string
    {
        return $memberId . ":" . $cardId . ':' . $time . 'supergeheimwachtwoord';
    }

    public static function isCardSecretValid(string $hashedSecret, string $memberId, string $cardId, string $time): bool
    {
        $secretInvalidBefore = time() - 7 * 24 * 60 * 60; // 7 days ago
        if ($time < $secretInvalidBefore) {
            return false;
        }

        $secret = self::generateCardSecret($memberId, $cardId, $time);
        return password_verify($secret, $hashedSecret);
    }


    /***************************************************************
     * AUTHENTICATION
     ***************************************************************/

    /**
     * @throws  CardNotRegisteredException
     * @throws  CannotRetrieveMemberException
     * @throws  MembershipStatusNotAllowedException
     */
    public static function authenticate(string $cardId)
    {
        self::logout();

        $dbm = new MemberDatabaseManager(env("member-database"));
        $memberId = $dbm->getCongressusMemberIdFromCardId($cardId);

        try {
            $member = createCongressusClient()->retrieveMember($memberId);
        } catch (\Exception) {
            throw new CannotRetrieveMemberException;
        }

        if (!self::isMembershipStatusAllowed($member->getStatus())) throw new MembershipStatusNotAllowedException;

        $authenticatedMember = AuthenticatedMember::createFromParentObject($member);

        if (is_int($authenticatedMember->getProfilePicture())) {
            $authenticatedMember->setProfilePicture(env("member.default-profile-picture"));
        }

        $authenticatedMember->setHasValidSddMandate(self::hasValidSddMandate($member));
        $authenticatedMember->setHasDutchBankAccount(substr($member->getBankAccount()->getIban(), 0, 2) == "NL");
        $authenticatedMember->setHasOverdueInvoice(self::hasOverdueInvoice($member));

        SessionManager::getAuthenticationSession()->setMember($authenticatedMember);
    }

    public static function logout(): void
    {
        SessionManager::clearModules();
    }

    private static function hasValidSddMandate(Member|MemberWithCustomFields $member): bool
    {
        foreach ($member->getBankAccount()->getSddMandates() as $mandate) {
            if ($mandate->getIsValid()) return true;
        }
        return false;
    }

    private static function hasOverdueInvoice(Member|MemberWithCustomFields $member): bool
    {
        $overdueInvoices = createCongressusClient()->listSaleInvoices(
            limit: 1,
            member_id: $member->getId(),
            invoice_status: ["late"],
            use_direct_debit: "0"
        );
        return !empty($overdueInvoices);
    }


    /***************************************************************
     * OTHER
     ***************************************************************/

    public static function isAuthenticated()
    {
        return !empty(SessionManager::getAuthenticationSession()->getMember());
    }

    public static function getAuthenticatedMember(): ?AuthenticatedMember
    {
        return SessionManager::getAuthenticationSession()->getMember();
    }

    private static function isMembershipStatusAllowed(MembershipStatus $membershipStatus): bool
    {
        return in_array($membershipStatus->getStatusId(), env('allowed-membership-status-ids'));
    }
}


enum RegistrationStage: string
{
    case SCAN_CARD = "SCAN_CARD";
    case ENTER_STUDENT_NUMBER = "ENTER_STUDENT_NUMBER";
    case CONFIRM_EMAIL = "CONFIRM_EMAIL";
}


// EXCEPTIONS

class UnknownRegistrationStage extends \Exception {}

class CardAlreadyRegisteredException extends \Exception {}

class CannotRetrieveMemberException extends \Exception {}

class MembershipStatusNotAllowedException extends \Exception {}

class EmailingFailedException extends \Exception {}
