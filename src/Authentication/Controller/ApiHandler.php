<?php

namespace Compucie\Touchscreen\Authentication\Controller;

use Compucie\Congressus\UserNotFoundException;
use Compucie\Database\Member\CardNotRegisteredException;
use Compucie\Database\Member\MemberDatabaseManager;
use Compucie\Touchscreen\Authentication\View\RegistrationAuthorizationPage;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;

use function Compucie\Touchscreen\env;

class ApiHandler extends \Compucie\Touchscreen\Main\Controller\ApiHandler
{
    protected function route(): void
    {
        $path = explode("?", $_SERVER['REQUEST_URI'])[0];
        match ($path) {
            "/authentication/logout" => $this->logout(),
            "/authentication/on-card-scan" => $this->onCardScan(),
            "/registration/activate-card" => $this->activateCard(),
            "/registration/search-member-to-register" => $this->searchMemberToRegister(),
            "/registration/send-activation-email" => $this->sendActivationEmail(),
        };
    }

    private function logout(): void
    {
        RfidAuthenticator::logout();
        $this->respond(200);
    }

    private function onCardScan(): void
    {
        try {
            try {
                $pageType = SessionManager::getPageType();
            } catch (\Throwable) {
                $pageType = "";
            }
            RfidAuthenticator::authenticate($_POST["card"]);
            $this->respond(200, $pageType);
        } catch (CardNotRegisteredException $th) {
            RfidAuthenticator::registerCard($_POST["card"]);
            $this->respond(201);
        } catch (CannotRetrieveMemberException) {
            $this->respond(490);
        } catch (CardAlreadyRegisteredException) {
            $this->respond(491);
        } catch (MembershipStatusNotAllowedException) {
            $this->respond(492);
        } catch (\mysqli_sql_exception) {
            $this->respond(590);
        } catch (\Throwable $th) {
            $this->respond(500, $th->getMessage());
        }
    }

    private function activateCard(): void
    {
        try {
            $memberId = @$_GET['memberId'];
            $cardId = @$_GET['cardId'];
            $time = @$_GET['time'];
            $hashedSecret = @$_GET['secret'];

            $dbm = new MemberDatabaseManager(env("member-database"));

            if ($dbm->isRfidCardRegistered($cardId)) {
                $title = "This card is already registered";
                $text = "Use the card to log in or register a different card";
                $this->show(RegistrationAuthorizationPage::page($title, $text));
            }

            if (!RfidAuthenticator::isCardSecretValid($hashedSecret, $memberId, $cardId, $time)) {
                $title = "The link is invalid 🫤";
                $text = "Maybe it has expired?";
                $this->show(RegistrationAuthorizationPage::page($title, $text));
            }

            $dbm->deleteMembersRfidRegistrations($memberId);
            $dbm->insertRfid($cardId, $memberId, isEmailConfirmed: true);
            $title = "Registration successful! 🎉";
            $text = "Welcome to the touchscreen";
            $this->show(RegistrationAuthorizationPage::page($title, $text));
        } catch (\Throwable) {
            $title = "Oops!";
            $text = "Something went wrong ☹️";
            $this->show(RegistrationAuthorizationPage::page($title, $text));
        }
    }

    private function searchMemberToRegister(): void
    {
        try {
            RfidAuthenticator::searchMemberToRegister($_POST["username"]);
            $this->respond(200);
        } catch (UserNotFoundException) {
            $this->respond(400);
        }
    }

    private function sendActivationEmail(): void
    {
        try {
            RfidAuthenticator::sendActivationEmail();
            $this->respond(200);
        } catch (\Throwable $th) {
            $this->respond(500, $th->getMessage());
        }
    }
}
