<?php

namespace Compucie\Touchscreen\Main\Controller;

use Compucie\Touchscreen\Main\Model\PageType;

use function Compucie\Touchscreen\env;

require_once("{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php");

SessionManager::getMainSession()->setPageType(PageType::HOME);

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Touchscreen</title>

	<link rel="icon" type="image/x-icon" href="<?php echo env("favicon"); ?>">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<!-- custom stylesheets -->
	<link href="public/css/styles.css" rel="stylesheet" type="text/css">
	<link href="public/css/poll.css" rel="stylesheet" type="text/css">

	<!-- scripts -->
	<script src="public/js/authentication.js" type="text/javascript"></script>
	<script src="public/js/event.page.js" type="text/javascript"></script>
	<script src="public/js/touchscreen.js" type="text/javascript"></script>
	<script src="public/js/shop.js" type="text/javascript"></script>
	<script src="public/js/jquery.rfid.js" type="text/javascript"></script>

	<script>
		getPage() // this is still necessary so the rfid scan is not called
	</script>
</head>

<body style="height: 100%; overflow: hidden">
	<div style="position: absolute; float: right; width: 1px; height: 1px; ">
		<form action="javascript:onCardScan();" id="loginForm" hidden>
			<input name="identifier" id="rfid_card"><br />
			<input type="submit">
			<form>
				<script type="text/javascript">
					$.rfidscan({
						parser: (rawData) => {
							if ((rawData.length != 9) && (rawData.length != 15)) return null;
							return rawData;
						},
						success: (cardData) => {
							if (cardData.length == 9) {
								$("#rfid_card").val(cardData.substr(0, 8));
								$("#loginForm").submit();
							} else if (cardData.length == 15) {
								$("#rfid_card").val(cardData.substr(0, 14));
								$("#loginForm").submit();
							}
						},
						error: () => {
							console.log("Bad Scan.");
						}
					});
				</script>
	</div>

	<div id="mainScreenDiv"></div>

	<!-- Used for custom popup messages -->
	<div id="errorModal" class="modal fade">
		<div class="modal-dialog modal-confirm">
			<div class="modal-content">
				<div class="modal-header">
					<div class="icon-box" id="warningBox">
						<i class="fas fa-exclamation"></i>
					</div>
				</div>
				<div class="modal-body text-center">
					<p id="errorText"></p>
				</div>
			</div>
		</div>
	</div>

	<div id="blockFrame" style="position: absolute; width: 100%; height: 100%; z-index: -2;"></div>
</body>

</html>