<?php

namespace Compucie\Touchscreen\Main\Controller;

use Compucie\Touchscreen\Authentication\Model\AuthenticationSession;
use Compucie\Touchscreen\Authentication\Model\RegistrationSession;
use Compucie\Touchscreen\Main\Model\MainSession;
use Compucie\Touchscreen\Main\Model\PageType;
use Compucie\Touchscreen\Main\Model\Session;
use Compucie\Touchscreen\ShopPage\Model\ShopSession;

class SessionManager
{
    public static function manage(): void
    {
        @session_start();

        self::getMainSession()->manage();

        foreach (self::getModuleSessions() as $session) {
            $session->manage();
        }
    }

    public static function clear(): void
    {
        @session_start();

        $_SESSION['mainSession'] = new MainSession; // first bc module sessions depend on main session
        self::clearModules();
    }

    public static function clearModules(): void
    {
        @session_start();

        $_SESSION['moduleSessions'] = [
            RegistrationSession::class => new RegistrationSession,
            AuthenticationSession::class => new AuthenticationSession,
            ShopSession::class => new ShopSession,
        ]; // module sessions may not depend on eachother so management order remains irrelevant
    }

    /**
     * @return  MainSession
     */
    public static function getMainSession(): MainSession
    {
        if (empty($_SESSION['mainSession'])) self::clear();
        return $_SESSION['mainSession'];
    }

    /**
     * @return  Session[]
     */
    private static function getModuleSessions(): array
    {
        if (empty($_SESSION['moduleSessions'])) self::clear();
        return $_SESSION['moduleSessions'];
    }

    public static function getPageType(): PageType
    {
        return self::getMainSession()->getPageType();
    }

    public static function setPageType(PageType $pageType): void
    {
        self::getMainSession()->setPageType($pageType);
    }

    public static function getRegistrationSession(): RegistrationSession
    {
        return self::get(RegistrationSession::class);
    }

    public static function getAuthenticationSession(): AuthenticationSession
    {
        return self::get(AuthenticationSession::class);
    }

    public static function getShopSession(): ShopSession
    {
        return self::get(ShopSession::class);
    }

    /**
     * @template    T
     * @param       class-string<T>   $sessionName
     * @return      T
     */
    public static function get($sessionName)
    {
        return self::getModuleSessions()[$sessionName];
    }
}
