<?php

namespace Compucie\Touchscreen\Main\Controller;

use Compucie\Congressus\ExtendedClient;

use function Compucie\Touchscreen\createCongressusClient;

abstract class ApiHandler
{
    private ExtendedClient $congressusClient;

    final public function __construct()
    {
        try {
            $this->congressusClient = createCongressusClient();
            session_start();
            $this->route();
        } catch (\Throwable) {
            http_response_code(500);
            $this->show(file_get_contents("{$_SERVER['DOCUMENT_ROOT']}/public/error/500.html"));
            exit(1);
        }
    }

    protected static function getUriPath(): string
    {
        return explode("?", $_SERVER['REQUEST_URI'])[0];;
    }

    protected abstract function route(): void;

    protected function respond(int $code, mixed $content = null): void
    {
        header("Content-type: application/json");
        echo json_encode(["code" => $code, "content" => $content]);
        exit(0);
    }

    protected function show(string $page): void
    {
        header("Content-type: text/html");
        echo $page;
        exit(0);
    }

    protected function getCongressusClient(): ExtendedClient
    {
        return $this->congressusClient;
    }
}
