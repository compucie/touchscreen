<?php

namespace Compucie\Touchscreen\Main\Controller;

use Compucie\Touchscreen\Authentication\View\RegistrationPage;
use Compucie\Touchscreen\EventPage\View\EventPage;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;
use Compucie\Touchscreen\Main\View\MainPage;
use Compucie\Touchscreen\ShopPage\View\ShopPage;

require_once("{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php");

SessionManager::manage();

echo match (SessionManager::getPageType()) {
    PageType::HOME => (new MainPage)->body(),
    PageType::CARD_REGISTRATION => RegistrationPage::generatePage(),
    PageType::EVENT => EventPage::generatePage(),
    PageType::SHOP => ShopPage::generatePage(),
};
