<?php

namespace Compucie\Touchscreen\Main\Model;

abstract class Session
{
    private array $vars;

    public function __construct()
    {
        $this->clear();
    }

    protected function get(string $name): mixed
    {
        return $this->getVars()[$name];
    }

    protected function set(string $name, mixed $value): void
    {
        $this->vars[$name] = $value;
    }

    protected function setNotNull(string $name, mixed $value): void
    {
        if (is_null($value)) return;
        $this->set($name, $value);
    }

    protected function unset(string $name): void
    {
        $this->set($name, null);
    }

    protected function getVars(): array
    {
        return $this->vars;
    }

    protected function setVars(array $vars): void
    {
        $this->vars = $vars;
    }

    public abstract function clear(): void;
    public abstract function manage(): void;
}
