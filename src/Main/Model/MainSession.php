<?php

namespace Compucie\Touchscreen\Main\Model;

use Compucie\Touchscreen\Main\Model\Session;

class MainSession extends Session
{
    public const PAGE_TYPE = "pageType";
    public const EVENT_ID = "eventId";

    public function clear(): void
    {
        $this->setVars([
            self::PAGE_TYPE => null,
            self::EVENT_ID => null,
        ]);
    }

    public function manage(): void
    {
        // Parse GET parameters
        $this->setNotNull(self::PAGE_TYPE, self::parsePageType());
        $this->setNotNull(self::EVENT_ID, $_GET[self::EVENT_ID] ?? null);

        // Cleanup
        if (self::getPageType() != PageType::EVENT) $this->unset(self::EVENT_ID);
    }

    private function parsePageType(): ?PageType
    {
        $string = strtoupper($_GET[self::PAGE_TYPE] ?? null);
        try {
            return PageType::from($string);
        } catch (\ValueError) {
            return self::getPageType() ?? PageType::HOME;
        }
    }

    public function getPageType(): ?PageType
    {
        return $this->get(self::PAGE_TYPE);
    }

    public function getEventId(): ?int
    {
        return $this->get(self::EVENT_ID);
    }

    public function setPageType(?PageType $pageType): void
    {
        $this->setNotNull(self::PAGE_TYPE, $pageType);
    }
}

enum PageType: string
{
    case HOME = "HOME";
    case EVENT = "EVENT";
    case SHOP = "SHOP";
    case CARD_REGISTRATION = "CARD_REGISTRATION";
}
