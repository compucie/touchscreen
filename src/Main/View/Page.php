<?php

namespace Compucie\Touchscreen\Main\View;

abstract class Page
{
    protected static function titleBar(string $text): string
    {
        return <<<HTML
            <div class="titleBar">
                $text
            </div>
        HTML;
    }

    protected static function formatCurrency(string $price): string
    {
        return "€" . number_format($price, 2, '.', ' ');
    }
}
