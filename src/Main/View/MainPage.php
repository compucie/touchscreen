<?php

namespace Compucie\Touchscreen\Main\View;

use Compucie\Touchscreen\EventOverview\View\EventOverview;
use Compucie\Touchscreen\Poll\View\PollCarousel;
use Compucie\Touchscreen\Puzzle\View\PuzzleMenu;
use Compucie\Touchscreen\Sidebar\View\Sidebar;
use Spatie\Async\Pool;

require_once("{$_SERVER['DOCUMENT_ROOT']}/vendor/autoload.php");

class MainPage extends Page
{
    private string $topTitleBar = "";
    private string $bottomTitleBar = "";
    private string $eventOverview = "";
    private string $puzzleMenu = "";
    private string $pollCarousel = "";
    private string $sidebar = "";

    public function body(): string
    {
        $pool = Pool::create();

        $pool->add(function () {
            $this->eventOverview = (new EventOverview)->eventOverview(); // (~ 400 ms) this is quite slow
        });

        $pool->add(function () {
            $this->puzzleMenu = PuzzleMenu::puzzleMenu(); // (~ 40 ms)
            $this->topTitleBar = self::titleBar("Events");
            $this->bottomTitleBar = self::titleBar("Puzzles");
            $this->pollCarousel = PollCarousel::pollCarousel();
            $this->sidebar = Sidebar::generateSidebar();
        });

        $pool->wait();

        return <<<HTML
            <main id="MainContent">

                <!-- TOP AREA -->
                <div id="EventArea">
                    {$this->topTitleBar}
                    {$this->eventOverview}
                    <span style="float: right; position: absolute; bottom: 0; right: 30px; z-index: -1; font-size: 8pt; color: gray;">
                        © Newton's CompuCie
                    </span>
                </div>

                <!-- Bottom area -->
                <div id="PuzzleArea">
                    {$this->bottomTitleBar}
                    {$this->puzzleMenu}
                    {$this->pollCarousel}
                </div>

            </main>

            {$this->sidebar}

            <script>
                $(function() {
                    $('.lazyload').each(function() {
                        $(this).attr('src', $(this).attr('data-src'));
                    });
                });
            </script>
        HTML;
    }
}
