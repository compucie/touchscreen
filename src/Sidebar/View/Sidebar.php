<?php

namespace Compucie\Touchscreen\Sidebar\View;

use Compucie\Touchscreen\Authentication\Controller\RfidAuthenticator;
use Compucie\Touchscreen\Main\Controller\SessionManager;
use Compucie\Touchscreen\Main\Model\PageType;

use function Compucie\Touchscreen\env;

class Sidebar
{
    const NAVIGATION_BUTTON_CLASS = "btn MenuButton border";

    const TEXT_REGISTER_CARD = "Register a card";
    const TEXT_LOGOUT = "Logout";
    const TEXT_GO_HOME = "Back";
    const TEXT_CANCEL = "Cancel";
    const TEXT_GO_SHOP = "Shop";
    const TEXT_SHOP_DISABLED = "Shop unavailable";

    public static function generateSidebar(): string
    {
        $image = self::image();
        $info = self::info();
        $menu = self::menu();

        return <<<HTML
            <aside id="Sidebar">
                <div id="SidebarContent">
                    $image
                    $info
                </div>
                <div id="SidebarMenu">
                    $menu
                </div>
            </aside>
        HTML;
    }

    private static function image(): string
    {
        if (RfidAuthenticator::isAuthenticated()) {
            $profilePicture = RfidAuthenticator::getAuthenticatedMember()->getProfilePicture();
            $imageSource = isset($profilePicture) ? $profilePicture->getUrl() : env("member.default-profile-picture");
            $style = "border-radius:50%; object-fit: cover;";
        } else {
            $imageSource = env("sidebar.logo");
            $style = "";
        }

        return <<<HTML
            <div id="UserPicture">
                <div class="insideBox" style="padding: 0;">
                    <img src="{$imageSource}" height="100%" width="100%" style="$style"/>
                </div>
            </div>
        HTML;
    }

    private static function info(): string
    {
        if (RfidAuthenticator::isAuthenticated()) {
            $id = "MemberName";
            $authenticatedMember = RfidAuthenticator::getAuthenticatedMember();
            if (!$authenticatedMember->hasDutchBankAccount()) {
                $text = "<br>Using the shop is restricted to Dutch bank accounts.";
            } elseif ($authenticatedMember->hasOverdueInvoice()) {
                $text = "<br>Using the shop is disallowed because of overdue invoices.";
            } else {
                $text = "<br>" . $authenticatedMember->getFirstName() . " " . $authenticatedMember->getLastName();
            }
        } else {
            $id = "AssociationName";
            $text = env("sidebar.name");
        }

        return <<<HTML
            <div id=$id>
                $text
            </div>
        HTML;
    }

    private static function menu(): string
    {
        if (SessionManager::getPageType() == PageType::HOME && !RfidAuthenticator::isAuthenticated()) {
            return self::cardScanInstruction();
        }

        if (SessionManager::getPageType() == PageType::CARD_REGISTRATION) {
            return self::navigationButton(PageType::HOME, self::TEXT_CANCEL);
        }

        return self::navigationButtons(SessionManager::getPageType());
    }

    private static function cardScanInstruction(): string
    {
        return <<<HTML
            <div class="card-scan-instruction">
                Scan any card to register or log in
            </div>
        HTML;
    }

    private static function navigationButtons(PageType $pageType): string
    {
        $buttons = array();
        switch ($pageType) {
            case PageType::HOME:

                // Top button
                $buttons[] = RfidAuthenticator::isAuthenticated()
                    ? self::logoutButton(self::TEXT_LOGOUT)
                    : "";

                // Bottom button
                if (self::isShopDisabled()) {
                    $buttons[] = self::disabledButton(self::TEXT_SHOP_DISABLED);
                } else {
                    $buttons[] = self::navigationButton(PageType::SHOP, self::TEXT_GO_SHOP);
                }

                break;
            case PageType::EVENT:
            case PageType::SHOP:

                // Top button
                $buttons[] = RfidAuthenticator::isAuthenticated()
                    ? self::logoutButton(self::TEXT_LOGOUT)
                    : "";

                // Bottom button
                $buttons[] = self::navigationButton(PageType::HOME, self::TEXT_GO_HOME);
                break;
        }

        return implode($buttons);
    }

    private static function navigationButton(PageType $destination, string $text): string
    {
        $class = self::NAVIGATION_BUTTON_CLASS;
        return <<<HTML
            <button class="$class" onclick="getPage('{$destination->value}')">
                {$text}
            </button>
        HTML;
    }

    private static function logoutButton(string $text): string
    {
        $class = self::NAVIGATION_BUTTON_CLASS;
        return <<<HTML
            <button class="$class" onclick="logout()">
                {$text}
            </button>
        HTML;
    }

    private static function disabledButton(string $text): string
    {
        $class = self::NAVIGATION_BUTTON_CLASS;
        return <<<HTML
            <button class="$class">
                {$text}
            </button>
        HTML;
    }

    private static function isNotLoggedIn(): bool
    {
        return !RfidAuthenticator::isAuthenticated();
    }

    private static function isShopDisabled(): bool
    {
        return !RfidAuthenticator::getAuthenticatedMember()->mayUseShop();
    }
}
