<?php

namespace Compucie\Touchscreen\Poll\View;

use Compucie\Database\Poll\Model\Poll;
use Compucie\Database\Poll\PollDatabaseManager;

use Compucie\Touchscreen\Authentication\Controller\RfidAuthenticator;

use function Compucie\Touchscreen\env;

class PollCarousel
{
    public static function pollCarousel(): string
    {
        try {
            $dbm = new PollDatabaseManager(env("poll-database"));
        } catch (\mysqli_sql_exception) {
            return "Couldn't connect to poll database";
        }
        $previousPoll = $dbm->getMostRecentlyExpiredPoll();
        $currentPoll = $dbm->getActivePolls()[0] ?? null;

        // HTML
        $backgroundImage = env("poll.background-image");
        $slides = self::slides($previousPoll, $currentPoll);
        $navigationButtons = self::navigationButtons();

        return <<<HTML
            <div class="area polls" style="background-image: url($backgroundImage);">
                <div class="carousel slide polls" id="carouselPolls" data-interval="false" data-ride="false">
                    $slides
                    $navigationButtons
                </div>
            </div>
        HTML;
    }

    /**
     * @param  Poll[] $polls
     * @return string
     */
    private static function slides(?Poll $previousPoll, ?Poll $currentPoll): string
    {

        $previousPollSlide = isset($previousPoll) ? self::pollSlide($previousPoll, false) : "";

        if (is_null($currentPoll)) {
            $currentPollSlide = <<<HTML
                <div class="carousel-item active" style="width: 100%; height: 100%;">
                    <div class="carousel-caption d-none d-md-block poll">
                        <div id="TextNoPoll">
                            There is currently no poll
                        </div>
                        <div id="TextPreviousPollInstruction">
                            Check out the results of the previous poll by clicking the arrow on the left.
                        </div>
                    </div>
                </div>
            HTML;
        } else {
            $currentPollSlide = self::pollSlide($currentPoll, true);
        }

        return <<<HTML
            <div class="carousel-inner" style="height: 100%;">
                $previousPollSlide
                $currentPollSlide
            </div>
        HTML;
    }

    private static function pollSlide(?Poll $poll, bool $isActive): string
    {
        $class = $isActive ? "carousel-item active" : "carousel-item";
        $question = $poll->getQuestion();
        $options = self::options($poll);

        $votingClosedElement =  ($isActive)
            ? ""
            : <<<HTML
                <span style="font-weight: normal;"> &nbsp [voting closed]</span>
            HTML;

        return <<<HTML
            <div class="{$class}" style="width: 100%; height: 100%;">
                <div class="carousel-caption d-none d-md-block poll">

                    <div class="pollContent">
                        <div id="TextNoPoll">
                            $question $votingClosedElement
                        </div>

                        <div class="pollOptions">
                            <form class="form poll" id="voteForm{$poll->getId()}">
                                <input type="hidden" name="pollId" value="{$poll->getId()}" />
                                $options
                            </form>
                        </div>
                    </div>

                    <script>
                        $(document).ready(function() {
                            $('#voteForm{$poll->getId()}').submit(function(e) {
                                e.preventDefault();
                                $.ajax({
                                    type: 'POST',
                                    cache: false,
                                    url: 'poll/cast-vote',
                                    data: $(this).serialize(),
                                    success: function (result) {
                                        switch (result.code) {
                                            case 201:
                                                $('#voteForm{$poll->getId()}').html(result.content);
                                                break;
                                            case 500:
                                                showPopup(result.content, 2500, 'error');
                                                break;
                                            default:
                                                break;
                                        }
                                    },
                                    error: function (result) {
                                        alert("Something went wrong");
                                    }
                                });
                            });
                        });
                    </script>
                </div>
            </div>
        HTML;
    }

    /**
     * Return the poll's options. Return it as a ballot if the authenticated member can vote.
     * Return it as a results list otherwise.
     */
    private static function options(Poll $poll): string
    {
        return self::userCanVote($poll) ? self::ballot($poll) : self::results($poll);
    }

    /**
     * Return the form on which the user can vote.
     */
    private static function ballot(Poll $poll): string
    {
        $optionsHtml = "";
        $options = $poll->getOptions();
        foreach ($options->getIds() as $id) {
            $percentage = ($poll->getVoteCount() != 0) ? round($options->getVoteCount($id) * 100 / $poll->getVoteCount()) : 0;

            $optionsHtml .= <<<HTML
                <div onclick="document.getElementById('answer{$id}').checked = true;" style="color: black;">
                    <div class="form-check">
                        <input class="form-check-input poll" type="radio" name='answerId' id="answer{$id}" value='{$id}'>
                        <label class="form-check-label poll">
                            {$options->getText($id)} <span style="float: right; text-align: right;"> {$options->getVoteCount($id)} ({$percentage}%) </span>
                        </label>
                    </div>
                    <div class="progress poll" id="progress{$id}">
                        <div class="progress-bar" role="progressbar" style="width: {$percentage}%;" aria-valuenow="{$options->getText($id)}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            HTML;
        }

        $memberId = RfidAuthenticator::getAuthenticatedMember()->getId();
        return <<<HTML
            <input type="hidden" name="memberId" value="$memberId" />
            {$optionsHtml}
            <input class="btn poll vote" type="submit" style="float: right;" value="Vote">
        HTML;
    }

    /**
     * Return the poll's (current) results.
     */
    public static function results(Poll $poll): string
    {
        $options = $poll->getOptions();
        $results = array();
        foreach ($options->getIds() as $id) {
            $percentage = ($poll->getVoteCount() != 0) ? round($options->getVoteCount($id) * 100 / $poll->getVoteCount()) : 0;

            $results[] = <<<HTML
                <div style="color: black;">
                    {$options->getText($id)} <span style="float: right; text-align: right;">{$options->getVoteCount($id)} ({$percentage}%)</span>
                    <div class="progress" id="progress{$id}">
                        <div class="progress-bar" role="progressbar" style="width: {$percentage}%;" aria-valuenow="{$options->getText($id)}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            HTML;
        }
        return implode($results);
    }

    private static function navigationButtons(): string
    {
        return <<<HTML
            <a class="carousel-control-prev" href="#carouselPolls" role="button" data-slide-to="0" onclick="this.blur()">
                <span class="carousel-control-prev-icon poll" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselPolls" role="button" data-slide-to="1" onclick="this.blur()">
                <span class="carousel-control-next-icon poll" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        HTML;
    }

    private static function userCanVote(Poll $poll): bool
    {
        if (!RfidAuthenticator::isAuthenticated()) {
            return false;
        }

        $dbm = new PollDatabaseManager(env("poll-database"));
        $memberId = RfidAuthenticator::getAuthenticatedMember()->getId();
        return in_array($poll->getId(), $dbm->getVotablePollIds($memberId));
    }
}
