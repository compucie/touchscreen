<?php

namespace Compucie\Touchscreen\Poll\Controller;

use Compucie\Database\Poll\PollDatabaseManager;

use Compucie\Touchscreen\Poll\View\PollCarousel;

use function Compucie\Touchscreen\env;

class ApiHandler extends \Compucie\Touchscreen\Main\Controller\ApiHandler
{
    protected function route(): void
    {
        match ($_SERVER['REQUEST_URI']) {
            "/poll/cast-vote" => $this->castVote(),
        };
    }

    private function castVote(): void
    {
        try {
            $dbm = new PollDatabaseManager(env("poll-database"));
            $dbm->addVote($_POST["pollId"], $_POST["memberId"], $_POST["answerId"]);
        } catch (\Throwable) {
            $this->respond(500, "Something went wrong with casting the vote.");
        }

        try {
            $poll = $dbm->getPoll($_POST["pollId"]);
            $content = PollCarousel::results($poll);
            $this->respond(201, $content);
        } catch (\Throwable) {
            $this->respond(500, "Something went wrong with displaying the results.");
        }
    }
}
