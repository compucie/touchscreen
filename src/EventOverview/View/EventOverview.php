<?php

namespace Compucie\Touchscreen\EventOverview\View;

use Compucie\Congressus\Model\Event;
use Compucie\Database\Event\EventDatabaseManager;
use Spatie\Async\Pool;

use function Compucie\Touchscreen\convertTimeZone;
use function Compucie\Touchscreen\createCongressusClient;
use function Compucie\Touchscreen\env;

class EventOverview
{
    private array $pinnedEventIds = array();
    private array $events = array();

    public function eventOverview(): string
    {
        $pool = Pool::create();

        // Pinned events (instant)
        $pool->add(function () {
            try {
                $this->pinnedEventIds = (new EventDatabaseManager(env("event-database")))->getCurrentlyPinnedEventIds();
            } catch (\mysqli_sql_exception) {
                return "Couldn't connect to poll database";
            }
        });

        // Upcoming events (~ 450 ms)
        $pool->add(function () {
            $this->events = createCongressusClient()->listUpcomingEvents(100);
        });

        $pool->wait();

        $pinnedEventCards = array();
        $unpinnedEventCards = array();
        foreach ($this->events as $event) {
            if (in_array($event->getId(), $this->pinnedEventIds)) {
                $pinnedEventCards[] = self::eventCard($event, true);
            } else {
                $unpinnedEventCards[] = self::eventCard($event, false);
            }
        }

        $eventOverview = implode("", array_merge($pinnedEventCards, $unpinnedEventCards));

        return <<<HTML
            <div class="overviewEvents">
                $eventOverview
            </div>
        HTML;
    }

    private static function eventCard(Event $event, bool $isPinned = false): string
    {
        if ($isPinned) {
            $pinned = "pinned";
            $pinIcon = <<<HTML
                <div class="titleBox pin">
                    <object data="public/img/pin.svg" type="image/svg+xml"></object>
                </div>
            HTML;
        } else {
            $pinned = "";
            $pinIcon = "";
        }

        $titleBoxTitle = $event->getName();
        $titleBoxStart = convertTimeZone($event->getStart())->format('j M H:i');
        $eventImageUrl = self::getEventImageUrl($event);

        return <<<HTML
            <div class="borderedWrapper eventCard" onclick="getEventPage({$event->getId()})">
                <div class="borderedWrapped border $pinned"></div>
                <div class="borderedWrapped eventCard $pinned">
                    <div class="titleBox eventCard">
                        $pinIcon
                        <div class="titleBox text">
                            <b>$titleBoxTitle</b><br>
                            $titleBoxStart
                        </div>
                    </div>
                    <img class="img eventCard"
                        src="$eventImageUrl">
                </div>
            </div>
        HTML;
    }

    private static function getEventImageUrl(Event $event): string
    {
        $media = $event->getMedia();

        if (is_null($media)) return "";

        foreach ($media as $entry) {
            if ($entry->getIsImage()) {
                return $entry->getUrl();
            }
        }
        return "";
    }
}
