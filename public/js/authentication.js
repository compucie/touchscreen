function onCardScan() {
	showPopup('Processing...', 60000, 'loading');
	$.ajax({
		type: "POST",
		url: "authentication/on-card-scan",
		data: {
			card: $("#rfid_card").val()
		},
		success: function (result) {
			switch (result.code) {
				case 200:
					(result.content == "CARD_REGISTRATION") ? getPage("HOME") : getPage();
					showPopup('Logged in!', 500, 'success')
					break;
				case 201:
					getPage("CARD_REGISTRATION");
					showPopup('Scan successful!', 1000, 'success');
					break;
				case 490:
					showPopup('Cannot retrieve the account connected to this card', 2000, 'warning');
					break;
				case 491:
					showPopup('This card is already registered', 2000, 'error');
					break;
				case 492:
					showPopup('You can no longer use the touchscreen', 2000, 'error');
					break;
				case 590:
					showPopup('Could not connect to database', 2000, 'warning');
					break;
				default:
					showPopup('Something unexpected happened on the server', 2000, 'warning');
					console.log('Unknown status code: ' + result.code);
					break;
			}
		},
		error: function () {
			showPopup('No server connection', 2000, 'warning');
		}
	});
}

function searchMemberToRegister(username) {
	showPopup('Searching...', 60000, 'loading');
	$.ajax({
		type: "POST",
		url: "registration/search-member-to-register",
		data: {
			username: username
		},
		success: function (result) {
			switch (result.code) {
				case 200:
					getPage();
					showPopup('Member found!', 750, 'success');
					break;
				case 400:
					getPage();
					showPopup("That member doesn't exist 🤡", 1500, 'error');
					break
				default:
					showPopup("We couldn't find that member :/", 1000, "error");
					break;
			}
		},
		error: function () {
			showPopup("We couldn't find that member :/", 2000, "error");
		}
	});
}

function sendRegistrationEmail() {
	showPopup('Sending...', 60000, 'loading');
	$.ajax({
		type: "POST",
		url: "registration/send-activation-email",
		success: function (result) {
			switch (result.code) {
				case 200:
					getPage("HOME");
					showPopup('Mail sent. Check your inbox.', 5000, 'success');
					break;
				default:
					console.log(result);
					showPopup('Something went wrong when sending the activation mail.', 2000, 'error');
					break;
			}
		},
		error: function (result) {
			console.log(result);
			showPopup('Something went wrong when sending the activation mail.', 2000, 'error');
		}
	});
}

function logout(afterPurchase = false) {
	showPopup('Sending...', 60000, 'loading');
	$.ajax({
		type: "POST",
		url: "authentication/logout",
		success: function () {
			getPage("HOME");
			if (!afterPurchase) showPopup('Logged out', 500, 'success');
		},
		error: function () {
			showPopup('Something went wrong when logging you out.', 3000, 'error');
		}
	});
}
