function showProductList(selectedFolderId) {
	$.ajax({
		type: "GET",
		url: "shop/product-list",
		data: {
			selectedFolderId: selectedFolderId,
		},
		success: function (result) {
			switch (result.code) {
				case 200:
					$('#EventsWrapper').replaceWith(result.content);
					break;
				default:
					break;
			}
		},
		error: function (result) {
			showPopup('The shop is currently not working ☹️', 1500, 'warning');
			console.log(result);
		},
	});
}

/**
 * Add the given product to the basket and update the shopping basket HTML.
 * @param {string} product JSON representation of the Product object
 */
function addProductToBasket(product) {
	$.ajax({
		type: "POST",
		url: "shop/add-product-to-basket",
		data: {
			product: product,
		},
		success: function (result) {
			switch (result.code) {
				case 200:
					$('#shoppingBasketContainer').replaceWith(result.content);
					break;
				default:
					showPopup('The shop is currently not working ☹️', 1500, 'warning');
					break;
			}
		},
		error: function () {
			showPopup('The shop is currently not working ☹️', 1500, 'warning');
		},
	});
}

/**
 * Remove the given product from the basket and update the shopping basket HTML.
 * @param {string} product JSON representation of the Product object
 */
function removeProductFromBasket(product) {
	$.ajax({
		type: "POST",
		url: "shop/remove-product-from-basket",
		data: {
			product: product,
		},
		success: function (result) {
			switch (result.code) {
				case 200:
					$('#shoppingBasketContainer').replaceWith(result.content);
					break;
				default:
					showPopup('The shop is currently not working ☹️', 1500, 'warning');
					break;
			}
		},
		error: function () {
			showPopup('The shop is currently not working ☹️', 1500, 'warning');
		},
	});
}

/**
 * Clear the basket and update the shopping basket HTML.
 */
function clearBasket() {
	$.ajax({
		type: "POST",
		url: "shop/clear-basket",
		success: function (result) {
			switch (result.code) {
				case 200:
					$('#shoppingBasketContainer').replaceWith(result.content);
					break;
				default:
					showPopup('The shop is currently not working ☹️', 1500, 'warning');
					break;
			}
		},
		error: function () {
			showPopup('The shop is currently not working ☹️', 1500, 'warning');
		},
	});
}

/**
 * Purchase the basket, log out and go back to the home screen.
 */
function purchaseBasket() {
	showPopup('Purchasing...', 60000, 'loading');
	$.ajax({
		type: "POST",
		url: "shop/purchase-basket",
		success: function (result) {
			logout(true);
			switch (result.code) {
				case 200:
					showPopup('Purchase successful!', 2000, 'success');
					break;
				default:
					showPopup('Purchase failed! Please inform the board member behind the counter.', 2000, 'error');
					break;
			}
		},
		error: function () {
			showPopup('Purchase failed! Please inform the board member behind the counter.', 2000, 'error');
		},
	});
}
