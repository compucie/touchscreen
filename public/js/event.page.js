function getEventPage(eventId) {
    $.ajax({
        type: "GET",
        url: "page/update",
        data: {
            pageType: "EVENT",
            eventId: eventId,
        },
        success: function (result) {
            $("#mainScreenDiv").html(result);
            $("#blockFrame").css("zIndex", -1);
        },
        error: function () {
            alert("Something went wrong");
        },
    });
}

function signupForEvent(eventId, eventParticipation) {
    showPopup('Processing...', 60000, 'loading');
    $.ajax({
        type: "POST",
        url: "event/subscribe",
        data: {
            eventId: eventId,
            eventParticipation: eventParticipation,
        },
        success: function (result) {
            switch (result.code) {
                case 201:
                    getPage();
                    showPopup('Signed up!', 1000, 'success');
                    break;
                default:
                    console.log(result.content);
                    showPopup('Something went wrong. Please try again by using the website.', 3000, 'error');
                    break;
            }
        },
        error: function (result) {
            console.log(result)
            showPopup('Something went wrong. Please try again by using the website.', 3000, 'error');
        }
    });
}

function cancelSignupForEvent(eventId, memberId) {
    showPopup('Processing...', 60000, 'loading');
    $.ajax({
        type: "POST",
        url: "event/unsubscribe",
        data: {
            eventId: eventId,
            memberId: memberId,
        },
        success: function (result) {
            switch (result.code) {
                case 201:
                    getPage();
                    showPopup('Sign-up cancelled', 1000, 'success');
                    break;
                default:
                    console.log(result.content)
                    showPopup('Something went wrong. Please try again by using the website.', 3000, 'error');
                    break;
            }
        },
        error: function (result) {
            console.log(result)
            showPopup('Something went wrong. Please try again by using the website.', 3000, 'error');
        }
    });
}
