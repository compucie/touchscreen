function getPage(pageType) {
	$("#blockFrame").css("zIndex", 15);

	$.ajax({
		type: "GET",
		url: "page/update",
		data: {
			pageType: pageType,
		},
		success: function (result) {
			$("#mainScreenDiv").html(result);
			$("#blockFrame").css("zIndex", -1);
		},
		error: function (xmlhttprequest, textstatus, message) {
			console.log(xmlhttprequest, textstatus, message);
			if (textstatus == "timeout") {
				showPopup("Request timed out", 2000, 'warning');
			} else {
				showPopup(textstatus + message, 3000, 'error');
			}
			$("#blockFrame").css("zIndex", -1);
		},
		timeout: 50000
	});
}

function showPopup(message, messageLength = 2000, messageType) {
	if (messageType == 'warning') {
		warning = '<i class="fas fa-exclamation"></i>';
		$('.modal-header').css("background", "#FFCC00");
	}
	if (messageType == 'info') {
		warning = '<i class="fas fa-exclamation"></i>';
		$('.modal-header').css("background", "#17A2B8");
	}
	if (messageType == 'error') {
		warning = '<i class="fas fa-times"></i>';
		$('.modal-header').css("background", "red");
	}
	if (messageType == 'success') {
		warning = '<i class="fas fa-check"></i>';
		$('.modal-header').css("background", "#489a2b");
	}
	if (messageType == 'loading') {
		warning = '<i class="fas fa-cog fa-spin"></i>';
		$('.modal-header').css("background", "gray");
	}
	MessageLength = messageLength;
	$('#errorText').html(message);
	$('#warningBox').html(warning);
	$('#errorModal').modal("show");
	$('#errorModal').data('hideInterval', setTimeout(function () {
		$('#errorModal').modal('hide');
	}, messageLength));
}

/**
 * Create an iframe with the printable content and open the browser's print dialog. Clean up the iframe afterwards.
 * @param {string} puzzleSrc The location of the puzzle image.
 */
function openPrintDialog(puzzleSrc) {
	showPopup('Loading...', 60000, 'loading');

	const iframe = document.createElement('iframe');
	iframe.setAttribute("id", "puzzleframe");
	iframe.setAttribute("src", "src/Puzzle/View/PuzzleIframe.php?puzzleSrc=" + puzzleSrc);
	iframe.setAttribute("hidden", true);
	iframe.setAttribute("name", "pdf");

	iframe.onload = () => {
		iframe.contentWindow.focus();
		iframe.contentWindow.print();
		showPopup('Finished', 1000, 'success');
	}

	document.body.appendChild(iframe);
	iframe.contentWindow.onafterprint = function () { document.body.removeChild(iframe); }
}
