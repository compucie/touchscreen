## Configuration

### .env
```
favicon = "public/img/newton.ico"

[datetime]
timezone = "Europe/Amsterdam"

[congressus]
token = "mysecrettoken"

[member-database]
hostname = "localhost"
username = "user"
password = "supersecret"
database = "member"

[event-database]
hostname = "localhost"
username = "user"
password = "supersecret"
database = "event"

[poll-database]
hostname = "localhost"
username = "user"
password = "supersecret"
database = "poll"

[sale-database]
hostname = "localhost"
username = "user"
password = "supersecret"
database = "sale"

[allowed-membership-status-ids]
0 = 1000
1 = 1001
2 = 1002
3 = 1003

[sidebar]
logo = "public/img/newton.svg"
name = "W.S.G. Isaac Newton"

[member]
default-profile-picture = "public/default-pfp.png.webp"

[poll]
background-image = "public/poll-background.jpg"

[rfid-email]
host = "smtp.gmail.com"
username = "user@example.com"
password = "supersecret"

sender-address = "user@example.com"
sender-name = "Newton's Compucie"

activation-email = "resources/authentication/rfid-authorization-email.html"

[puzzle]
service-location = "https://puzzles.example.com"
logo = "newton.png"

[store-product-folder-slugs]
0 = "snacks"
1 = "drinks"
2 = "merch"
```